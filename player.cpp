#include "player.h"

player::player(int x, int y):QGraphicsEllipseItem(x,y,10,10){
    this->x = x;
    this->y = y;
}

int player::getVida()
{
    return vida;
}

int player::getX()
{
    return x;
}

int player::getY()
{
    return y;
}

void player::hitted()
{
    vida -= 1;
}


