# GraficaPOO

## Description
GraficaPOO es juego PvP (jugador contra jugador) por turnos, en cual se situan a 2 jugadores en un plano de 2 dimensiones, para ganar hay que eliminar al jugador contrario utilizando funciones matemáticas como modo de ataque. Las funciones se grafican desde la posición desde el jugador que ataca, si la función graficada toca al jugar contrario, éste quedará eliminado, si falla le tocará al otro jugador, así hasta que algún jugador quede eliminado.


## V1.0

Esta primera versión, se implementó un plano, 2 jugadores (equipo rojo y azul), y una sola funcion de ataque (y=x).
El plano tiene coordenadas (x,y), siendo el 0,0 la esquina superior izquierda y el 600,600 la esquina inferior derecha. Al iniciar el juego, son generados 2 jugadores, uno del equipo rojo de color rojo (equipo 1), y otro del equipo azul de azul (equipo 2). Los jugadores son puestos en una posición al azar dentro del plano con coordenadas x=y. Para atacar tienen la función x=y, la cual traza una linea desde la posción del jugador hasta el punto x=y, si esta linea logra atravezar al jugador enemigo el atacante será ganador, de lo contrario será el turno del jugador contrario.

## Instalación

- Tener QT en su versión 5.
- Importar los archivos a una carpeta común.
- Desde QT Creator importar el proyecto seleccionando el archivo proyecto.pro
- Con el proyecto importado presionar el boton "Run".
- Jugar.


## Authors and acknowledgment
- Carlos Beytía        201921048-3
- Nicolas Nazar        202030539-0
- Vicente Olmos        202030529-3
- Nicolas Ruiz         201930570-0
- Fernando Carrasco    202030542-0
## Video Presentación 
https://youtu.be/0s2Z7jPFsXk

