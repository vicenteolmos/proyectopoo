#include "graphwar.h"
#include "ui_graphwar.h"


graphwar::graphwar(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::graphwar){
    ui->setupUi(this);
    scene = new QGraphicsScene(0,0,600,600);

    int random1 = QRandomGenerator::global()->bounded(10);
    int random2 = QRandomGenerator::global()->bounded(10);

    player1 = new playerRed(50*random1,50*random1);
    player2 = new playerBlue(50*random2,50*random2);

    scene->addItem(player1);
    scene->addItem(player2);

    ui->view->setScene(scene);
    connect(ui->function,SIGNAL(pressed()),this,SLOT(turnos()));

}

graphwar::~graphwar(){
    delete scene;
    delete player1;
    delete player2;
    delete linea;
    delete ui;

}

void graphwar::turnos(){
    QMessageBox ganador;
    int pos = (ui->posicion->text()).toInt();

    if(turno == 1){
        attack(player1,player2,pos);
        turno++;

    }
    else{
        attack(player2,player1,pos);
        turno--;
    }

    if(player1->getVida() == 0){
        ganador.setText("Gana jugador 2");
        ganador.exec();
    }
    if(player2->getVida() == 0){
        ganador.setText("Gana jugador 1");
        ganador.exec();
    }
}



void graphwar::attack(player *jugador,player *jugador2,int pos){

    if(jugador->getVida() == 1){
        linea = new graph(jugador->getX(),jugador->getY(),pos,pos);
        scene->addItem(linea);
        if(jugador->getX() < jugador2->getX()){
                if(pos >= jugador2->getX())
                    jugador2->hitted();
            }
        if(jugador->getX() > jugador2->getX()){
                if(pos <= jugador2->getX())
                    jugador2->hitted();
            }
    }
}


